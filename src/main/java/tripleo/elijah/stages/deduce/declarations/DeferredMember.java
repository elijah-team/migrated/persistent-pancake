/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce.declarations;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.Eventual;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.VariableStatement;
import tripleo.elijah.stages.deduce.IInvocation;
import tripleo.elijah.stages.deduce.percy.DeduceTypeResolve2;
import tripleo.elijah.stages.gen_fn.GenType;
import tripleo.elijah.stages.gen_fn.GeneratedNode;

/**
 * Created 6/27/21 1:41 AM
 */
public class DeferredMember {
	private final OS_Element parent;
	private final IInvocation invocation;
	private final VariableStatement variableStatement;
	private final Eventual<GenType> typePromise = new Eventual<GenType>();
	private final Eventual<GeneratedNode> externalRef = new Eventual<GeneratedNode>();
	private final DeduceTypeResolve2                        resolver;

	public DeferredMember(final OS_Element aParent, final IInvocation aInvocation, final VariableStatement aVariableStatement, final DeduceTypeResolve2 aResolver) {
		parent = aParent;
		invocation = aInvocation;
		variableStatement = aVariableStatement;
		resolver = aResolver;
	}

	public @NotNull Eventual<GenType> typePromise() {
		return typePromise;
	}

	public OS_Element getParent() {
		return parent;
	}

	public IInvocation getInvocation() {
		return invocation;
	}

	public VariableStatement getVariableStatement() {
		return variableStatement;
	}

	// for DeducePhase
	public @NotNull Eventual<GenType> typeResolved() {
		return typePromise;
	}

	public Eventual<GeneratedNode> externalRef() {
		return externalRef;
	}

	public @NotNull Eventual<GeneratedNode> externalRefDeferred() {
		return externalRef;
	}

	@Override
	public @NotNull String toString() {
		return "DeferredMember{" +
				"parent=" + parent +
				", variableName=" + variableStatement.getName() +
				'}';
	}

	public DeduceTypeResolve2 getResolver() {
		return resolver;
	}
}

//
//
//
