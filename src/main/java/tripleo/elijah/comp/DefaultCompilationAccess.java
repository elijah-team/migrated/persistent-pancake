package tripleo.elijah.comp;

import jdk.jfr.Event;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.Eventual;
import tripleo.elijah.comp.internal.ProcessRecord;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.testing.comp.IFunctionMapHook;

import java.util.List;

public class DefaultCompilationAccess implements ICompilationAccess {
	protected final Compilation             compilation;
	private final   Eventual<AccessBus>     _p_accessBus     = new Eventual<>();
	private final   Eventual<ProcessRecord> _p_ProcessRecord = new Eventual<>();

	public DefaultCompilationAccess(final Compilation aCompilation) {
		compilation = aCompilation;

//		compilation.setCompilationAccess(this);
	}

	@Override
	@NotNull
	public ElLog.Verbosity testSilence() {
		//final boolean isSilent = compilation.silent; // TODO No such thing. silent is a local var
		final boolean isSilent = false; // TODO fix this

		return isSilent ? ElLog.Verbosity.SILENT : ElLog.Verbosity.VERBOSE;
	}

	@Override
	public Compilation getCompilation() {
		return compilation;
	}

	@Override
	public void writeLogs() {
		final boolean     silent = testSilence() == ElLog.Verbosity.SILENT;
		final List<ElLog> logs   = compilation._elLogs();

		CA_writeLogs.apply(silent, logs, compilation);
	}

	@Override
	public List<IFunctionMapHook> functionMapHooks() {
		return compilation.getDeducePhase().functionMapHooks;
	}

	@Override
	public void setProcessRecord(final ProcessRecord aProcessRecord) {
		_p_ProcessRecord.resolve(aProcessRecord);
	}

	@Override
	public void setAccessBus(final AccessBus aAb) {
		_p_accessBus.resolve(aAb);
	}

	@Override
	public Stages getStage() {
		return getCompilation()._cfg().stage;
	}
}
